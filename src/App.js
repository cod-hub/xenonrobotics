import logo from "./logo.svg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Login } from "./Pages/Login/Login";
import { SelectionRelease } from "./Pages/Selection/SelectionRelease";
import { AdvanceInput } from "./components";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
// import "style.css";
function App() {
  return (
    <Router>
      <div className="App">
        {/* <AdvanceInput /> */}
        <Switch>
          <Route path="/selection">
            <SelectionRelease />
          </Route>
          <Route path="/">
            <Login />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
