import { Button, Form, Row, Col, Image } from "react-bootstrap";
import "./login.scss";
import { CommonInput, Layout } from "../../components";
import React, { useState } from "react";
import * as $ from "jquery";

function Login() {
  const [email, setemail] = useState("");
  const [pass, setpass] = useState("");

  return (
    <Layout>
      <div className="login">
        <div className="loginmain">
          <div className="welcome">
            <Image src="/images/xenongold.svg" />
            <p className="welcomehead">Welcome Back</p>
            <p className="welcomelicence">
              Enter your license key and email to continue
            </p>
          </div>
          <Form>
            <Row>
              <Col md={12}>
                <CommonInput
                  label="E-Mail Address"
                  place="example@website.com"
                  email
                  setEmail={setemail}
                  // onChange={({ target: { value } }) => {
                  //   setemail(value);
                  // }}
                />
              </Col>
              <Col md={12}>
                <CommonInput
                  label="License Key"
                  place="XXXXXX-XXXXXX-XXXXXX"
                  pass={true}
                  // setPass={setpass}
                  // onChange={({ target: { value } }) => {
                  //   setpass(value);
                  // }}
                />
              </Col>
            </Row>

            <Button
              className="submitbtn"
              variant=""
              style={{
                backgroundColor:
                  !email.trim() || !pass.trim() ? "#00000020" : "#E7BF8B",
                color: !email.trim() || !pass.trim() ? "#FFFFFF25" : "#000000",
              }}
            >
              Continue
            </Button>
          </Form>
        </div>
      </div>
    </Layout>
  );
}

export { Login };
