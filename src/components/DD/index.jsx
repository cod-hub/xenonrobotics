import React from "react";
import "./drop.scss";
import { Form } from "react-bootstrap";

function Drop({ label, pass, place, ...rest }) {
  return (
    <>
      {/* <Form> */}
      <div class="form-wrapper dropform">
        <div class="full-input">
          <label for="name">{label}</label>
          <select type={pass} name="name" {...rest}>
            <option className="opt">{place}</option>
            <option>num 1</option>
            <option>num 1</option>
            <option>num 1</option>
          </select>
        </div>
      </div>
      {/* </Form> */}
    </>
  );
}
export { Drop };
