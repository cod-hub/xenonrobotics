import React from "react";
import { Image } from "react-bootstrap";
import "./layout.scss";

function Layout({ children }) {
  return (
    <div className="layoutmain">
      <div className="topdiv">
        <Image src="/images/goldicon.svg" />
        <div className="versiondiv">
          <p className="versiontime">12:00:00</p>
          <p className="versioncontrol">Version 1.0.0</p>
        </div>
      </div>
      {children}
      <div className="bottomdiv">
        <div className="bottominner">
          <p className="copyright">&#169; 2021 Xenon Robotics</p>
          <div className="socialicon">
            <Image src="/images/twitter.svg" />
            <Image src="/images/insta.svg" className="instaicon" />
          </div>
        </div>
      </div>
    </div>
  );
}
export { Layout };
