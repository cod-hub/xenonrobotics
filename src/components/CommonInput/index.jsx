import React, { useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import "./commoninput.scss";
import * as $ from "jquery";

export function CommonInput({
  label,
  place,
  pass,
  email,
  onChange,
  setEmail,
  setPass,
  pat,
}) {
  const [license, setLicense] = useState("");
  const handler = (e) => {
    console.log(e, "handler");
  };

  const pattern = (target) => {
    console.log("patterndash", pat);
    if (target.value) {
      let value = target.value.split("-").join(""); // Remove dash (-) if mistakenly entered.
      console.log(value.length);
      if (value.length === 18) {
        $(
          ".activation-main .form-main .form-group .icon-Submit-Arrow"
        ).addClass("active");
      } else {
        $(
          ".activation-main .form-main .form-group .icon-Submit-Arrow"
        ).removeClass("active");
      }
      let finalVal = value.match(/.{1,6}/g).join("-");
      target.value = finalVal;
      setLicense({
        licence: {
          key: finalVal,
          email: license.email,
        },
      });
    } else {
      setLicense({
        licence: {
          key: target.value,
          email: license.email,
        },
      });
    }
  };

  const pattern2 = (target) => {
    if (target.value) {
      let value = target.value.split(":").join(""); // Remove dash (:) if mistakenly entered.
      if (value.length === 9) {
        $(
          ".activation-main .form-main .form-group .icon-Submit-Arrow"
        ).addClass("active");
      } else {
        $(
          ".activation-main .form-main .form-group .icon-Submit-Arrow"
        ).removeClass("active");
      }
      let finalVal = value.match(/.{1,2}/g).join(":");
      target.value = finalVal;
      setLicense({
        licence: {
          key: finalVal,
          email: license.email,
        },
      });
    } else {
      setLicense({
        licence: {
          key: target.value,
          email: license.email,
        },
      });
    }
  };
  // const changeEmail = ({ target }) => {
  //   if (
  //     license.key.length === 20 &&
  //     target.value.length > 5 &&
  //     target.value.split("@").length > 1 &&
  //     target.value.split("@")[1].length > 4
  //   ) {
  //     $(".activation-main .form-main .form-group .icon-Submit-Arrow").addClass(
  //       "active"
  //     );
  //   } else {
  //     $(
  //       ".activation-main .form-main .form-group .icon-Submit-Arrow"
  //     ).removeClass("active");
  //   }
  //   setLicense({
  //     license: {
  //       email: target.value,
  //       key: license.key,
  //     },
  //   });
  // };

  // const checkKey = async () => {
  //   if (
  //     machineId !== "" &&
  //     license.key.length === 20 &&
  //     license.email.length > 5 &&
  //     license.email.split("@").length > 1 &&
  //     license.email.split("@")[1].length > 4
  //   ) {
  //     let res = { eek: true };
  //     if (200 /* res.status */ === 200) {
  //       // chrome.storage.local.set({ license: this.state.license });
  //       setValidLicense({ validLicense: true });
  //       setLoginText({ loginText: "SUCCESS" });
  //       handler("home");
  //     } else if (res.status === 403) {
  //       setLoginText({ loginText: "Another computer is logged in." });
  //     } else if (res.status === 400) {
  //       setLoginText({ loginText: "Your key has expired" });
  //     } else {
  //       setLoginText({
  //         loginText:
  //           "Login Failed. Please proceed to the #setup guide for login instructions. Found via the Discord server.",
  //       });
  //     }
  //   } else {
  //     setLoginText({ loginText: "Enter a valid email and key" });
  //   }
  // };
  return (
    <div class="form-wrapper">
      <div class="full-input">
        <label for="name">{label}</label>
        {email ? (
          <input
            type="email"
            name="name"
            placeholder={place}
            onChange={({ target }) => {
              setEmail(target.value);
            }}
          />
        ) : (
          <input
            type="text"
            name="name"
            class="asdasd"
            placeholder={place}
            value={license.key}
            maxLength={20}
            onChange={({ target }) => {
              // pattern(target);
              // setPass(target.value);
              {
                pat === "patcol" ? pattern2(target) : pattern(target);
              }
            }}
          />
        )}
      </div>
    </div>
  );
}
