import React from "react";
import { RadioBtn } from "../Radiobtn";
import { Image } from "react-bootstrap";
import "./commondiv.scss";

function CommonDiv({ title, title2, img }) {
  return (
    <div className="commondivmain">
      <div className="logodiv">
        {img ? (
          <Image src="/images/puzzle.svg" className="image" />
        ) : (
          <div className="logo"></div>
        )}
      </div>
      <div className="desc">
        <p className="desctitle">{title}</p>
        <p className="desctitle2">{title2}</p>
      </div>
      {/* <RadioBtn id="custom-switch" /> */}
    </div>
  );
}

export { CommonDiv };
