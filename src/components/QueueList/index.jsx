import React from "react";
import { Image } from "react-bootstrap";
import "./queue.scss";

function QueueList({ productname, producttime }) {
  return (
    <div className="queuemain">
      <div className="imagediv">
        <Image src="/images/greenshoe.svg" className="queueimage" />
      </div>
      <div className="queueinfo">
        <p className="prodname">{productname}</p>
        <p className="prodtime">{producttime}</p>
      </div>
    </div>
  );
}

export { QueueList };
