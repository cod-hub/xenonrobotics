import React, { useEffect, useState } from "react";
import "./advance.css";

function AdvanceInput({ title, desc, options = [], onSelect = () => {} }) {
  const id = Date.now();
  useEffect(() => {
    const drop = document.querySelector("#drop-btn-" + id);
    const menu_wrapper = document.querySelector("#wrapper-" + id);
    const hiddenSelect = document.querySelector("#hidden-select-" + id);
    const menuLi = document.querySelectorAll("#menu-bar-" + id + " li");
    const headElement = document.querySelector(
      "#drop-btn-" + id + " p.headDesc"
    );
    document.addEventListener("mouseup", function (e) {
      var container = document.getElementById("drop-down-" + id);
      if (container)
        if (!container.contains(e.target)) {
          menu_wrapper.classList.remove("show");
        }
    });

    menuLi.forEach((item) => {
      item.onclick = (e) => {
        let title = e.target.innerHTML;
        let value = e.target.dataset.value;
        hiddenSelect.value = value;
        headElement.innerHTML = title;
        headElement.style.color = "#fff";
        onSelect({ title, value });
        menu_wrapper.classList.remove("show");
      };
    });

    drop.onclick = clickItem;
    function clickItem(e) {
      console.log(e.target, "e");
      menu_wrapper.classList.toggle("show");
    }
  }, []);

  return (
    <nav id={"drop-down-" + id}>
      <input type="hidden" name="size" value="" id={`hidden-select-${id}`} />
      <div className="drop-btn" id={`drop-btn-${id}`}>
        <label htmlFor="">{title}</label>
        <p className="headDesc">{desc}</p>
        <span className="fa fa-caret-down"></span>
      </div>
      <div className="wrapper" id={`wrapper-${id}`}>
        {options.length > 0 && (
          <ul className={"menu-bar"} id={`menu-bar-${id}`}>
            {options.map((v) => (
              <li data-value={v.value}>{v.title}</li>
            ))}
          </ul>
        )}
      </div>
    </nav>
  );
}
export { AdvanceInput };
