import React from "react";
import { Image } from "react-bootstrap";
import "./slider.scss";

export function SliderData() {
  return (
    <div className="slidermain">
      <div className="sliderimage">
        <Image src="/images/greenshoe.svg" />
      </div>
      <div className="info">
        <p className="infohead">Nike Dunk Low Green Glow (W)</p>
        <p className="infodate">04/16/2021 09:00:00AM (CST)</p>
      </div>
    </div>
  );
}
