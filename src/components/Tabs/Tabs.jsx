import React, { useEffect, useState } from "react";
import {
  Image,
  Row,
  Col,
  Form,
  Button,
  Dropdown,
  ProgressBar,
} from "react-bootstrap";
import "./tabs.scss";
import * as $ from "jquery";
import { CustomSlider, CommonInput, Drop } from "../../components";
import { CommonDiv } from "../Commondiv";
import { Layout } from "../Layout";
import { AdvanceInput } from "../AdvanceInput";
import { QueueList } from "../QueueList";

function Tabs({ variant, pat }) {
  const [timer, settimer] = useState("");
  const [size, setsize] = useState("");
  const [proxy, setproxy] = useState("");
  const [radiobtun, setradiobtun] = useState({});

  useEffect(() => {
    $(".tab-slider--body").hide();
    $(".tab-slider--body:first").show();

    $(".tab-slider--nav li").on("click", function () {
      $(".tab-slider--body").hide();
      var activeTab = $(this).attr("rel");
      $("#" + activeTab).fadeIn();
      if ($(this).attr("rel") === "tab2") {
        $(".tab-slider--tabs").addClass("slide");
      } else {
        $(".tab-slider--tabs").removeClass("slide");
      }
      $(".tab-slider--nav li").removeClass("active");
      $(this).addClass("active");
    });
    $(".toggle-password").on("click", function () {
      $(this).toggleClass("fa-eye fa-eye-slash");
      var input = $($(this).attr("toggle"));
      if (input.attr("type") === "password") {
        input.attr("type", "text");
      } else {
        input.attr("type", "password");
      }
    });
    // $("#custom-switch").on("change", function () {
    //   $("#custom-switch").addClass("radioback");
    // });
  }, []);

  return (
    <>
      <Layout>
        <div className="container-fluid">
          <div className="tab-slider--nav">
            <ul className="tab-slider--tabs">
              <li className="tab-slider--trigger active" rel="tab1">
                <span className="icon-shoe tabbtn"></span>
              </li>
              <li className="tab-slider--trigger" rel="tab2">
                {/* <Image src="/images/setting.svg" /> */}
                <span className="icon-black-settings-button tabbtn"></span>
              </li>
            </ul>
          </div>
          <div className="tab-slider">
            <div id="tab1" className="tab-slider--body">
              <Row>
                <Col md={8}>
                  <div className="firstdiv">
                    <p className="sliderhead">Select Release</p>
                    <div className="Slider-Section">
                      <CustomSlider />
                    </div>
                  </div>
                  <div className="prodsubmit">
                    <Form>
                      <div className="prodform">
                        <Row>
                          <Col md={4}>
                            <CommonInput
                              label="ACO Timer"
                              value={timer}
                              place="hh:mm:ss:mmm (optional)"
                              onChange={({ target: { value } }) => {
                                settimer(value);
                              }}
                              pat="patcol"
                            />
                          </Col>
                          <Col md={4}>
                            <AdvanceInput
                              title="Size"
                              desc="None"
                              onSelect={(obj) => {
                                console.log(obj, "selected");
                              }}
                              options={[
                                { title: "10", value: "10" },
                                { title: "10.1", value: "10.1" },
                                { title: "12", value: "12" },
                              ]}
                            />
                          </Col>
                          <Col md={4}>
                            <CommonInput
                              label="Proxy"
                              place="ip:port (optional)"
                              value={proxy}
                              // pat="patcol"
                              type="number"
                              // onChange={({ target: { value } }) => {
                              //   setproxy(value);
                              // }}
                            />
                          </Col>
                          <Col md={6}>
                            <Button
                              variant=""
                              style={{
                                backgroundColor:
                                  !timer.trim() || !proxy.trim() || !size.trim()
                                    ? "#1C1B1A"
                                    : "#00CC29",
                                color:
                                  !timer.trim() || !proxy.trim() || !size.trim()
                                    ? "#FFFFFF25"
                                    : "#ffff",
                              }}
                            >
                              Add Shoe
                            </Button>
                          </Col>
                          <Col md={6}>
                            <Button
                              variant=""
                              style={{
                                backgroundColor:
                                  !timer.trim() || !proxy.trim() || !size.trim()
                                    ? "#1C1B1A"
                                    : "#F91B60",
                                color:
                                  !timer.trim() || !proxy.trim() || !size.trim()
                                    ? "#FFFFFF25"
                                    : "#ffff",
                              }}
                            >
                              Remove Shoe
                            </Button>
                          </Col>
                        </Row>
                      </div>
                    </Form>
                  </div>
                </Col>
                <Col md={4}>
                  <div className="seconddiv">
                    <div className="secondheading">
                      <p>Queue</p>
                      <div className="dropdown">
                        <Dropdown>
                          <Dropdown.Toggle
                            variant=""
                            id="dropdown-basic"
                          ></Dropdown.Toggle>

                          <Dropdown.Menu>
                            <Dropdown.Item href="#/action-1">
                              Action
                            </Dropdown.Item>
                            <Dropdown.Item href="#/action-2">
                              Another action
                            </Dropdown.Item>
                            <Dropdown.Item href="#/action-3">
                              Something else
                            </Dropdown.Item>
                          </Dropdown.Menu>
                        </Dropdown>
                      </div>
                    </div>
                    <QueueList
                      productname="Nike Dunk Low Green Glow (W)"
                      producttime="04/16/2021 09:00:00AM (CST)"
                    />
                    <div className="itemdiv">
                      <div className="iteminnerdiv">
                        <Image src="/images/itemshoe.svg" />
                        <p className="productname">No Items</p>
                        <p className="productdesc">
                          Items added from the release list will be shown here
                        </p>
                      </div>
                    </div>
                    <Button
                      className="generatebtn"
                      style={{
                        backgroundColor:
                          !timer.trim() || !proxy.trim() || !size.trim()
                            ? "#1C1B1A"
                            : "#F91B60",
                        color:
                          !timer.trim() || !proxy.trim() || !size.trim()
                            ? "#FFFFFF25"
                            : "#ffff",
                      }}
                    >
                      Generate
                    </Button>
                  </div>
                </Col>
              </Row>
            </div>
            <div id="tab2" className="tab-slider--body">
              <div className="settingstabmain">
                <div className="settingheading">
                  <p>Settings</p>
                  <div className="dropdown">
                    <Dropdown>
                      <Dropdown.Toggle
                        variant=""
                        id="dropdown-basic"
                      ></Dropdown.Toggle>

                      <Dropdown.Menu>
                        <Dropdown.Item href="#/action-1">Action</Dropdown.Item>
                        <Dropdown.Item href="#/action-2">
                          Another action
                        </Dropdown.Item>
                        <Dropdown.Item href="#/action-3">
                          Something else
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                </div>

                <div className="companion">
                  <div className="planimage">
                    <p>Aa</p>
                  </div>
                  <div className="planinfo">
                    <p className="planheading">Xenon SNKRS Companion</p>
                    <p className="planrenewal">Renewal Plan</p>
                    <ProgressBar now={60} />
                    <p className="plandays">plan renews in 15 days</p>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-md-12 eyeinput">
                    <input
                      id="password-field"
                      type="password"
                      className="form-control"
                      name="password"
                      defaultValue=""
                    />
                    <span
                      toggle="#password-field"
                      className="fa fa-fw fa-eye field-icon toggle-password"
                    ></span>
                    <span
                      toggle=""
                      className="fa fa-fw fa-clone copy-icon toggle-password"
                    ></span>
                  </div>
                </div>

                <hr className="line" />
                <div className="togglediv">
                  <CommonDiv
                    title="Toggle Title"
                    title2="Lorem ipsum dolor sit amet"
                  />
                  <Form>
                    <Form.Check
                      type="switch"
                      id="custom-switch"
                      onChange={(e) => {
                        setradiobtun({
                          ...radiobtun,
                          customSwitch1: e.target.checked,
                        });
                      }}
                      className={radiobtun["customSwitch1"] ? "radioback" : ""}
                    />
                  </Form>
                </div>

                <div className="togglediv">
                  <CommonDiv
                    title="Toggle Title"
                    title2="Lorem ipsum dolor sit amet"
                    img
                  />
                  <Form>
                    <Form.Check
                      type="switch"
                      id="custom-switch2"
                      onChange={(e) => {
                        setradiobtun({
                          ...radiobtun,
                          customSwitch2: e.target.checked,
                        });
                      }}
                      className={radiobtun["customSwitch2"] ? "radioback" : ""}
                    />
                  </Form>
                </div>
                <hr className="line" />

                <AdvanceInput
                  title="Size"
                  desc="None"
                  id="setting"
                  options={[
                    { title: "10", value: "10" },
                    { title: "10.1", value: "10.1" },
                    { title: "12", value: "12" },
                  ]}
                />
                <AdvanceInput
                  title="Size"
                  desc="None"
                  id="setting"
                  options={[
                    { title: "10", value: "10" },
                    { title: "10.1", value: "10.1" },
                    { title: "12", value: "12" },
                  ]}
                />
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
}
export { Tabs };
