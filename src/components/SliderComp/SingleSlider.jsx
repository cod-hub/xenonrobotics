import React from "react";
import "./CustomSlider.css";

export function SingleSlider({ bgcolor, data, icolor }) {
  return (
    <div className="slider-box">
      <div className="Slider-innerBox" style={{ backgroundColor: bgcolor }}>
        <div className="imagediv">
          <img src="/images/greenshoe.svg" alt="" className="user-image" />
        </div>
        <div className="Slider-TextBox">
          <p className="Slider-para">{data.para}</p>
        </div>
        <div className="Slider-Foot">
          <div className="Slider-Foot-Text">
            {/* <p className="text">{data.name}</p> */}
            <p className="foot-heading">{data.subname}</p>
          </div>
          {/* <div className="Comma-Image quote" style={{ color: icolor }}>
            {/* <div className="Comma-Image quote" style={{color:bgcolor+'50'}}> */}
          {/* <img src={require('../../assets/images/comma.svg')} alt="" className='Comma-image'/> */}
          {/* </div> */}
        </div>
      </div>
    </div>
  );
}
