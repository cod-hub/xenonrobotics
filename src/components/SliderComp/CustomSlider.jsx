import React, { Component } from "react";
import Slider from "react-slick";
import "./CustomSlider.css";
import { SingleSlider } from "./SingleSlider";

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block" }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block" }}
      onClick={onClick}
    />
  );
}

export class CustomSlider extends Component {
  render() {
    const settings = {
      dots: false,
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: true,
            centerPadding: "40px",
          },
        },
      ],
      prevArrow: <SamplePrevArrow />,
      nextArrow: <SampleNextArrow />,
    };
    const color = [
      //   "#5F3B96",
      //   "#E8417C",
      //   "#42A078",
      // '#721D0A',
    ];
    const iconcolor = [
      //   "rgba(131, 52, 251, 0.5)",
      //   "rgba(241, 179, 201, 0.5)",
      //   "rgba(86, 196, 148, 0.5)",
      // '#721DfA50',
    ];
    const slider = [
      {
        para: "Nike Dunk Low Green Glow (W)",
        subname: "04/16/2021 09:00:00AM (CST)",
      },
      {
        para: "Nike Dunk Low Green Glow (W)",
        subname: "04/16/2021 09:00:00AM (CST)",
      },
      {
        para: "Nike Dunk Low Green Glow (W)",
        subname: "04/16/2021 09:00:00AM (CST)",
      },
      {
        para: "Nike Dunk Low Green Glow (W)",
        subname: "04/16/2021 09:00:00AM (CST)",
      },
      {
        para: "Nike Dunk Low Green Glow (W)",
        subname: "04/16/2021 09:00:00AM (CST)",
      },
    ];

    return (
      <Slider {...settings} className="slider">
        {slider.map((val, ind) => {
          //   const coloind = ind % color.length;
          // console.log(coloind);
          return (
            <SingleSlider
              key={ind + "slider"}
              //   icolor={iconcolor[coloind]}
              //   bgcolor={color[coloind]}
              data={val}
            />
          );
        })}

        {/* <div className="slider-box">
            <div className='Slider-innerBox'>
            <img src={require('../../assets/images/joesph.svg')} alt="" className='user-image'/>
            <div className='Slider-TextBox'>
            <p className='Slider-para h5'>The ease of use is one of the best things I like about Gigy. The gigas are eager to work and I am always satisfied delegating my work to them.</p>
            </div>
            <div className='Slider-Foot'>
            <div className='Slider-Foot-Text'>
              <p className="text">Marcus Walter</p>
              <p className='h5'>ELDERLY-CARE-GIVER</p>
            </div>
            <img src={require('../../assets/images/comma.svg')} alt="" className='Comma-image'/>
            </div>
            </div>
          </div> */}
      </Slider>
    );
  }
}
